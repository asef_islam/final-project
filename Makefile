# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

#Links the chess executable
chess: King.o Rook.o Pawn.o Knight.o Bishop.o CreatePiece.o Queen.o main.o Chess.o Board.o
	$(CC) -o chess King.o Rook.o Pawn.o Knight.o Bishop.o CreatePiece.o Queen.o main.o Chess.o Board.o 

#Compiles main.cpp into an opject file
main.o: Chess.h main.cpp
	$(CC) $(CFLAGS) -c main.cpp

#Links the test1 executable
test1: test1.o King.o Rook.o Pawn.o Knight.o Bishop.o CreatePiece.o Queen.o
	$(CC) -o test1 test1.o King.o Rook.o Pawn.o Knight.o Bishop.o CreatePiece.o Queen.o  

# Compiles test1.cpp into an object file
test1.o: test1.cpp King.h Rook.h Pawn.h Knight.h Bishop.h Queen.h
	$(CC) $(CFLAGS) -c test1.cpp 

#Links the test2 executable
test2: test2.o CreatePiece.o Chess.o 
	$(CC) -o test2 test2.o CreatePiece.o Board.o Chess.o  King.o Rook.o Pawn.o Knight.o Bishop.o Queen.o

# Compiles test2.cpp into an object file
test2.o: test2.cpp Chess.h CreatePiece.h
	$(CC) $(CFLAGS) -c test2.cpp

#Links the test3 executable
test3: test3.o CreatePiece.o Board.o
	$(CC) -o test3 test3.o CreatePiece.o Board.o  King.o Rook.o Pawn.o Knight.o Bishop.o Queen.o

# Compiles test3.cpp into an object file
test3.o: test3.cpp Board.h CreatePiece.h
	$(CC) $(CFLAGS) -c test3.cpp

#Links the test4 executable
test4: test4.o CreatePiece.o Chess.o Board.o
	$(CC) -o test4 test4.o CreatePiece.o Board.o Chess.o  King.o Rook.o Pawn.o Knight.o \
Bishop.o Queen.o

# Compiles test4.cpp into an object file
test4.o: test4.cpp Chess.h CreatePiece.h Board.h
	$(CC) $(CFLAGS) -c test4.cpp

# Compiles Board.cpp into an object file
Board.o: Board.h Board.cpp CreatePiece.h Terminal.h CreatePiece.o Chess.o
	$(CC) $(CLAGS) -c Board.cpp

# Compiles CreatePiece.cpp into an object file
CreatePiece.o: CreatePiece.h CreatePiece.cpp
	$(CC) $(CFLAGS) -c CreatePiece.cpp

# Compiles Chess.cpp into an object file
Chess.o: Chess.h Chess.cpp CreatePiece.h 
	$(CC) $(CLAGS) -c Chess.cpp

# Compiles King.cpp into an object file
King.o: King.h King.cpp
	$(CC) $(CLAGS) -c King.cpp

# Compiles Rook.cpp into an object file
Rook.o: Rook.h Rook.cpp
	$(CC) $(CLAGS) -c Rook.cpp

# Compiles Pawn.cpp into an object file                                                                                                                  
Pawn.o: Pawn.h Pawn.cpp
	$(CC) $(CLAGS) -c Pawn.cpp

# Compiles Knight.cpp into an object file                                                                                                                
Knight.o: Knight.h Knight.cpp
	$(CC) $(CLAGS) -c Knight.cpp

# Compiles Queen.cpp into an object file                                                                                                                 
Queen.o: Queen.h Queen.cpp
	$(CC) $(CLAGS) -c Queen.cpp

# Compiles Bishop.cpp into an object file                                                                                                                
Bishop.o: Bishop.h Bishop.cpp
	$(CC) $(CLAGS) -c Bishop.cpp

# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o test1  *.gcov
