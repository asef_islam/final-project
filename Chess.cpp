#include "Chess.h"
#include "CreatePiece.h"
#include <iostream>

using std::cout;
using std::endl;
using std::make_pair;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

Chess::Chess(const Chess& chess) {
  //looping through all board positions
  for( char r='8' ; r>='1' ; r-- ) {
     for( char c='A' ; c<='H' ; c++ ) {
       std::pair<char,char> loc = std::make_pair(c,r);
       //each piece at each location temporarily saved
       const Piece* piece = chess.board()(loc);
       if(piece != NULL) { //piece exists at location
	 //piece is added to the copy board
	 _board.add_piece(loc, piece->to_ascii());
       }
     }
  }
  //setting chess copy's _turn_white
  _turn_white = chess.turn_white();
}

Board& Chess::get_board() {
  return _board;
}

void Chess::set_turn_white(bool white) {
  if(white) {
    _turn_white = true;
  }
  else {
    _turn_white = false;
  }
}

bool Chess::move_check( std::pair< char , char > start , std::pair< char , char > end ) {
const Piece* piece = _board.operator()(start);
  if(!piece) { //piece null, no piece at the starting location
     return false;
  }
  if(piece->is_white() != turn_white()) { //it's not your piece
     return false;
  }
  const Piece* endpiece = _board.operator()(end); //piece at ending location
  if(endpiece) { //there is a piece at the end                   
    if(piece->is_white() == endpiece->is_white()) { //same team                
      return false;
    }
    else { //different team, piece captured                                               
      if(piece->legal_capture_shape(start,end) && _board.path_is_clear(start,end)) {
	_board.remove_piece(end);
	delete _board(end);
        _board.add_piece(end, piece->to_ascii());
	_board.remove_piece(start);
	delete _board(start); 
      }
      else {
        return false;
      }
    }
  }
else { //no piece at end                                            
  if(piece->legal_move_shape(start, end) && _board.path_is_clear(start,end)) {
    _board.add_piece(end, piece->to_ascii());
    _board.remove_piece(start);
    delete _board(start);
    _board.add_piece(end, piece->to_ascii());
    _board.remove_piece(start);
    delete _board(start);
  }
    else {
      return false;
    }
  }
  if(piece->to_ascii() == 'P') { //white Pawn                                    
    if(end.second=='8') { //made it to other side                                
      _board.remove_piece(end);
      delete _board(end); 
      _board.add_piece(end, 'Q');
    }
  }
  if(piece->to_ascii() == 'p') { //black Pawn                    
    if(end.second=='1') { //made it to other side                                 
      _board.remove_piece(end);
      delete _board(end); 
      _board.add_piece(end, 'q');
    }
  }
  set_turn_white(!turn_white()); //switch turn                            
  return true;
}  
  
bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end ) {
  Chess copy(*this);
  if(copy.move_check(start,end)) { //move is exectuted on copy board
    if(copy.in_check(!copy.turn_white())) { //if it puts this person in check, can't do the move
      return false;
    }
  }
  return move_check(start,end); 
}


bool Chess::in_check( bool white ) const {
  Piece* king;
  const Piece* temp;
  const Piece* p;
  
  //are we looking for white or black king?
  if (white) {
    king = create_piece('K');
  }
  else  {
    king = create_piece('k');
  }
  std::pair<char, char> king_pos;

  //finds where this player's king is 
  for( char r='8' ; r>='1' ; r-- )  {
      for( char c='A' ; c<='H' ; c++ )	{
	king_pos = make_pair(c,r);
	temp = _board.operator()(king_pos);
	if(temp != NULL) {
	  if (temp->to_ascii() == king->to_ascii()) { //piece at location is the specified king
	    goto label; //loop is exited
	  }
	}
      }
  }
 label:
 std::pair<char, char> current_pos;
 for( char r='8' ; r>='1' ; r-- ) {
   for( char c='A' ; c<='H' ; c++) {
       current_pos = make_pair(c,r);
       p = _board(current_pos);
       if(p != NULL) { //piece is at location
	 if(p->is_white() != king->is_white()) { //piece is the opposite color from the king being checked
	   //this piece can capture the king, king is in check
	   if(p->legal_capture_shape(current_pos, king_pos) && _board.path_is_clear(current_pos, king_pos)) {
	     delete king;
	     return true;
	   }
	 }
       }
   }
 }
 delete king;
 return false;
}


bool Chess::in_mate( bool white ) const
{
  if(this->in_check(white)) { //checkmate only checked is player's king is already in check
  const Piece* p;
  std::pair<char, char> current_pos;
  std::pair<char, char> end;

  //looping through all possible positions
  for( char r='8' ; r>='1' ; r-- ) {
    for( char c='A' ; c<='H' ; c++) {
      current_pos = make_pair(c,r);
      p = _board(current_pos);
      if(p != NULL && (p->is_white() == white)) { //piece exists at location and is player's piece
	for( char r_end='8' ; r_end>='1' ; r_end-- ) {
	   for( char c_end='A' ; c_end<='H' ; c_end++) {
	     end = make_pair(c_end,r_end);
	     //checking each piece's possible moves
	       if(p->legal_move_shape(current_pos, end) && _board.path_is_clear(current_pos, end)) {
		 Chess matecheck(*this);
		 //move is executed on copy board and status of check is determined
		 if(matecheck.make_move(current_pos, end)) { //make_move returned true, meaning move won;t cause check
		   return false; //not checkmate
		 }
		 
	       }
	   }
	 }
      }
   }
  }
  return true;
  }
  return false; 
}

bool Chess::in_stalemate( bool white ) const
{
  const Piece* p;
  std::pair<char, char> current_pos;
  std::pair<char, char> end;

  //looping through all possible positions
  for( char r='8' ; r>='1' ; r-- ) {
    for( char c='A' ; c<='H' ; c++) {
      current_pos = make_pair(c,r);
      p = _board(current_pos); //each location 
      if(p != NULL && (p->is_white() == white)) { //piece exists at location and is player's piece
	for( char r_end='8' ; r_end>='1' ; r_end-- ) {
	  for( char c_end='A' ; c_end<='H' ; c_end++) {
	    end = make_pair(c_end,r_end); //each location
	    //checking each piece's possible moves
	    if (p->legal_move_shape(current_pos, end) && _board.path_is_clear(current_pos, end)) {
	      Chess stalecheck(*this);
	      if(stalecheck.make_move(current_pos, end)) { //a move can be made by player's piece
		  return false;
		}
	      }
	  }
	}
      }
    }
  }
  return true; 
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

std::istream& operator >> ( std::istream& is , Chess& chess )
{
  //looping through whole board
  for( char r='8' ; r>='1' ; r-- )
  {
    for( char c='A' ; c<='H' ; c++ )
      {
	//clears chess board
	chess.get_board().remove_piece(make_pair(c,r));
	char piece;
	is >> piece;
	if(piece != '-') { //there's a piece there
	  chess.get_board().add_piece(make_pair(c,r),piece); //adds loaded pieces
	}
      }
  }
  char turn;
  is >> turn;
  //turn will be set
  if(turn == 'w') {
    chess.set_turn_white(true);
  }
  if(turn == 'b') {
    chess.set_turn_white(false);
  }
}
