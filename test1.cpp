//test1.cpp

#include <iostream>
#include <cassert>
#include "Board.h"
#include "Piece.h"
#include "Pawn.h"
#include "King.h"
#include "Queen.h"
#include "Bishop.h"
#include "Rook.h"
#include "Knight.h"
#include "CreatePiece.h"
#include <utility>

using std::pair;
using std::make_pair; 

int main() {
  //Test moves for King
  Piece* white_king = create_piece('K');
  pair<char, char> start = make_pair('F','5');
  pair<char, char> end = make_pair('F','6'); 
  assert(white_king->legal_move_shape(start, end));
  end = make_pair('E','6');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('G','6');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('F','4');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('E','5');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('E','4');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('G','5');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('G','4');
  assert(white_king->legal_move_shape(start,end));
  end = make_pair('F','7');
  assert(!white_king->legal_move_shape(start,end));
  end = make_pair('F','3');
  assert(!white_king->legal_move_shape(start,end));
  end = make_pair('D','5');
  assert(!white_king->legal_move_shape(start,end));
  end = make_pair('D','7');
  assert(!white_king->legal_move_shape(start,end));


  //Test moves for rook
  Piece* white_rook = create_piece('R');
  start = make_pair('D','5');
  end = make_pair('D','6');
  assert(white_rook->legal_move_shape(start, end));
  end = make_pair('D','8');
  assert(white_rook->legal_move_shape(start,end));
  end = make_pair('D','4');
  assert(white_rook->legal_move_shape(start,end));
  end = make_pair('A','5');
  assert(white_rook->legal_move_shape(start,end));
  end = make_pair('E','5');
  assert(white_rook->legal_move_shape(start,end));
  end = make_pair('D','1');
  assert(white_rook->legal_move_shape(start,end));
  end = make_pair('G','5');
  assert(white_rook->legal_move_shape(start,end));
  end = make_pair('G','4');
  assert(!white_rook->legal_move_shape(start,end));
  end = make_pair('F','7');
  assert(!white_rook->legal_move_shape(start,end));
  end = make_pair('E','4');
  assert(!white_rook->legal_move_shape(start,end));
  end = make_pair('C','4');
  assert(!white_rook->legal_move_shape(start,end));

  //Test moves for bishop
  Piece* white_bishop = create_piece('B');
  end = make_pair('C','6');
  assert(white_bishop->legal_move_shape(start,end));
  end = make_pair('B','7');
  assert(white_bishop->legal_move_shape(start,end));
  end = make_pair('E','6');
  assert(white_bishop->legal_move_shape(start,end));
  end = make_pair('H','1');
  assert(white_bishop->legal_move_shape(start,end));
  end = make_pair('B','3');
  assert(white_bishop->legal_move_shape(start,end));
  end = make_pair('D','6');
  assert(!white_bishop->legal_move_shape(start,end));
  end = make_pair('C','5');
  assert(!white_bishop->legal_move_shape(start,end));
  end = make_pair('E','5');
  assert(!white_bishop->legal_move_shape(start,end));
  end = make_pair('H','2');
  assert(!white_bishop->legal_move_shape(start,end));
  end = make_pair('D','3');
  assert(!white_bishop->legal_move_shape(start,end));
  Piece* black_bishop = create_piece('b');
  start = make_pair('F','8');
  end = make_pair('B','4');
  assert(black_bishop->legal_move_shape(start,end));
  
  //Test moves for queen
  Piece* white_queen = create_piece('Q');
  start = make_pair('D','4');
  end = make_pair('C','4');
  assert(white_queen->legal_move_shape(start,end));
  end = make_pair('F','4');
  assert(white_queen->legal_move_shape(start,end));
  end = make_pair('B','6');
  assert(white_queen->legal_move_shape(start,end));
  end = make_pair('D','1');
  assert(white_queen->legal_move_shape(start,end));
  end = make_pair('C','3');
  assert(white_queen->legal_move_shape(start,end));
  end = make_pair('C','6');
  assert(!white_queen->legal_move_shape(start,end));
  end = make_pair('B','5');
  assert(!white_queen->legal_move_shape(start,end));
  end = make_pair('F','5');
  assert(!white_queen->legal_move_shape(start,end));
  end = make_pair('H','6');
  assert(!white_queen->legal_move_shape(start,end));
  end = make_pair('E','2');
  assert(!white_queen->legal_move_shape(start,end));
  start = make_pair('D','1');
  end = make_pair('E','8');
  assert(!white_queen->legal_move_shape(start,end));
  
  //Test moves for knight
  Piece* white_knight = create_piece('N');
  start = make_pair('D','4');
  end = make_pair('C','6');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('B','5');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('B','3');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('C','2');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('E','6');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('F','5');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('F','3');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('E','2');
  assert(white_knight->legal_move_shape(start,end));
  end = make_pair('C','5');
  assert(!white_knight->legal_move_shape(start,end));
  end = make_pair('D','5');
  assert(!white_knight->legal_move_shape(start,end));
  end = make_pair('E','4');
  assert(!white_knight->legal_move_shape(start,end));
  end = make_pair('C','3');
  assert(!white_knight->legal_move_shape(start,end));
  end = make_pair('B','2');
  assert(!white_knight->legal_move_shape(start,end));

  //Test moves for pawn
  Piece* white_pawn = create_piece('P');
  start = make_pair('A','2');
  end = make_pair('G','3');
  assert(!white_pawn->legal_move_shape(start,end));
  //assert(white_pawn->legal_move_shape(start,end));
  end = make_pair('F','4');
  assert(white_pawn->legal_move_shape(start,end));
  end = make_pair('E','3');
  assert(!white_pawn->legal_move_shape(start,end));
  end = make_pair('E','2');
  assert(!white_pawn->legal_move_shape(start,end));
  end = make_pair('F','5');
  assert(!white_pawn->legal_move_shape(start,end));
  end = make_pair('F','1');
  assert(!white_pawn->legal_move_shape(start,end));
  start = make_pair('B','4');
  end = make_pair('B','6');
  assert(!white_pawn->legal_move_shape(start,end));
  Piece* black_pawn = create_piece('p');
  start = make_pair('F','7');
  end = make_pair('F','6');
  assert(black_pawn->legal_move_shape(start,end));
  end = make_pair('F','5');
  assert(black_pawn->legal_move_shape(start,end));
  end = make_pair('G','6');
  assert(!black_pawn->legal_move_shape(start,end));
  end = make_pair('E','7');
  assert(!black_pawn->legal_move_shape(start,end));
  end = make_pair('F','4');
  assert(!black_pawn->legal_move_shape(start,end));
  end = make_pair('F','8');
  assert(!black_pawn->legal_move_shape(start,end));
  start = make_pair('B','4');
  end = make_pair('B','5');
  assert(!black_pawn->legal_move_shape(start,end));  

  //Test legal capture for pawn
  start = make_pair('F','2');
  end = make_pair('E','3');
  assert(white_pawn->legal_capture_shape(start,end));
  end =	make_pair('G','3');
  assert(white_pawn->legal_capture_shape(start,end));
  end =	make_pair('F','3');
  assert(!white_pawn->legal_capture_shape(start,end));
  end = make_pair('E','1');
  assert(!white_pawn->legal_capture_shape(start,end));
  end = make_pair('G','2');
  assert(!white_pawn->legal_capture_shape(start,end));
  end =	make_pair('E','1');
  assert(black_pawn->legal_capture_shape(start,end));
  end = make_pair('G','1');
  assert(black_pawn->legal_capture_shape(start,end));
  end = make_pair('F','3');
  assert(!black_pawn->legal_capture_shape(start,end));
  end = make_pair('E','3');
  assert(!black_pawn->legal_capture_shape(start,end));
  end = make_pair('G','2');
  assert(!black_pawn->legal_capture_shape(start,end));
  std::cout << "All assertions passed!" << std::endl;

  
}
