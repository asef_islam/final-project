#include "Queen.h"
#include <utility>
#include <cmath>

using std::pair;
using std::abs;

bool Queen::legal_move_shape( pair< char , char > start , pair< char , char > end ) const {

  char start_row = start.second;
  char start_col = start.first;
  char end_col = end.first;
  char end_row = end.second;
  unsigned int col_diff = abs(end_col - start_col);
  unsigned int row_diff = abs(end_row - start_row);

  // checks if start is in range                                                                                      
  if ((start_row < '1' || start_row > '8') || (start_col < 'A' || start_col > 'H')) {
    return false;
  }
  // checks if end is in range                                                                                        
  if ((end_row < '1' || end_row > '8') || (end_col < 'A' || end_col > 'H')) {
    return false;
  }

  //valid movements for queen to make
  if(((start_col > end_col || start_col < end_col) &&  start_row == end_row) ||
     ((start_row > end_row || start_row < end_row) &&  start_col == end_col) ||
     (col_diff == row_diff)) {
    return true;
  }
  else {
    return false;
  }

}
