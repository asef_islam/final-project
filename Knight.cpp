//Knight.cpp

#include "Knight.h"
#include <utility>
#include <cmath>

//Determines if starting and ending location represent a legal move for a knight
bool Knight::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //starting and ending locations must be different
  if(start == end) {
    return false;
  }
  //checks that end location is within the board boundaries
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  //conditions for a knight move to be valid
  //end location is 2 spaces away horizontally and 1 space away vertically, or vice versa
  if((abs(start.first- end.first) == 1 && abs(start.second-end.second)==2) || (abs(start.first- end.first) == 2 && abs(start.second-end.second)==1)) {
    return true;
  }
  else { //conditions not met, move is invalid
    return false;
  }
}
