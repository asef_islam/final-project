//Bishop.cpp

#include "Bishop.h"
#include <utility>
#include <cmath> 

//Determines if starting and ending location represent a legal move for a bishop
bool Bishop::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //starting and ending locations must be different
  if(start == end) {
    return false;
  }
  //checks that end location is within the board boundaries
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  //conditions for a bishop move to be valid
  //it is diagonal, i.e. the distance moved horizontally = distance moved vertically
  if(abs(start.first-end.first) == abs(start.second-end.second)) {
    return true;
  }
  else { //conditions not met, move is invalid
    return false;
  }
}

