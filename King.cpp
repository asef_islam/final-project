//King.cpp

#include "King.h"
#include <string>
#include <utility>

using std::string;

//returns whether or not move is valid for a king
bool King::legal_move_shape(std::pair< char , char > start , std::pair< char , char > end ) const {
  //starting and ending locations must be different
  if(start == end) {
    return false;
  }
  //checks that end location is within the board boundaries
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  //conditions for a king move to be valid
  //end location is one space away in any direction, including diagonal
  if((start.first+1 == end.first || start.first-1 == end.first || start.first == end.first) &&
     (start.second+1 == end.second || start.second-1 == end.second || start.second == end.second)) {
    return true;
  }
  else { //conditions not met, move is invalid
    return false;
  }
}
