//Pawn.cpp

#include "Pawn.h"
#include <utility>
#include <cmath>


//Determines if starting and ending location represent a legal move for a pawn
bool Pawn::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //starting and ending locations must be different
  if(start == end) {
    return false;
  }
  //checks that end location is within the board boundaries
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  //conditions for a pawn move to be valid
  //moves one step vertically towards opponent's side of the board
  //can move 2 steps only if this is the first move
  if(is_white()) {
    if (start.second == '2') { //has not moved, can move 1 or 2 spaces
      if (end.first - start.first == 0 && end.second - start.second == 2) { //moves two steps forward
	return true;
      }
      else if (end.first - start.first == 0 && end.second - start.second == 1) {
	return true;
      }
    }
    else { //has moved from original location
      if (end.first - start.first == 0 && end.second - start.second == 1) {
	return true;
      }
      else {
	return false;
      }
    }
  }
  else { //black 
    if (start.second=='7') { //has not moved
      if (end.first - start.first == 0 && start.second - end.second == 2) {
	return true;
      }
      else if (end.first - start.first == 0 && start.second - end.second == 1) {
        return true;
      }
    }
    else { //has moved from original location
      if (end.first - start.first == 0 && start.second - end.second == 1) {
	return true;
      }
      else {  
	return false;
      }
    }
  }
  return false;
}

bool Pawn::legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  if(start == end) {
    return false;
  }
  
  if(start.first < 'A' || start.first > 'H' || start.second < '1' || start.second > '8') {
    return false;
  }
  
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  if (is_white()) { //pawn is white, diagonal movement upward
    if (abs(end.first-start.first)==1 && end.second-start.second==1) {
      return true;
    }
    else {
      return false;
    }
  }
  else {// pawn is black, diagonal movement downwards
    if (abs(start.first-end.first) ==1 && start.second-end.second==1) {
      return true;
    }
    else {
      return false;
    }
  }
  
}
