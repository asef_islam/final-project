/////////////////////////////////
// DO NOT MODIFY THIS FILE!!!! //
/////////////////////////////////
#ifndef MYSTERY_H
#define MYSTERY_H

#include "Piece.h"
#include <iostream>

class Mystery : public Piece
{
public:
  bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
	{
	  std::cout << "yES" << std::endl;
		//starting and ending locations must be different
  if(start == end) {
    return false;
  }
  //checks that end location is within the board boundaries
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  //conditions for a knight move to be valid
  //end location is 2 spaces away horizontally and 1 space away vertically, or vice versa
  if((abs(start.first- end.first) == 2 && abs(start.second-end.second)==3) || (abs(start.first- end.first) \
== 3 && abs(start.second-end.second)==2)) {
    std::cout << "t" << std::endl;
    return true;
  }
  else { //conditions not met, move is invalid
    std::cout << "h" << std::endl;
    return false;
  }
	}

	char to_ascii( void ) const { return is_white() ? 'M' : 'm'; }

private:
	Mystery( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // MYSTERY_H
