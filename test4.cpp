#include "CreatePiece.h"
#include <utility>
#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include "Chess.h"

using std::make_pair;
using std::pair;
using std::cout;
using std::endl;
using std::cin; 
using std::string;


int main() {
  Chess chess;
  string argument = "check.txt";
  std::ifstream ifs;
  ifs.open( argument );
  ifs >> chess;
  cout << "BOARD IN CHECK" << endl; 
  chess.board().display();
  assert(chess.in_check(false)); 
  Chess pre_promote;
  argument = "pre_promotion.txt";
  std::ifstream ppi; 
  ppi.open(argument);
  ppi >> pre_promote;
  cout << "WHITE PAWN ABOUT TO GET PROMOTED" << endl; 
  pre_promote.board().display();
  pre_promote.make_move(make_pair('F','7'),make_pair('F','8'));
  cout << "WHITE PAWN PROMOTED TO QUEEN" << endl;
  pre_promote.board().display(); 
}
