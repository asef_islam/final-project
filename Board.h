#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <map>
#include "Piece.h"
#include "Pawn.h"
#include "Mystery.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include <utility> 

class Board
{
	// Throughout, we will be accessing board positions using an std::pair< char , char >.
	// The assumption is that the first value is the row with values in {'A','B','C','D','E','F','G','H'} (all caps)
	// and the second is the column, with values in {'1','2','3','4','5','6','7','8'}

public:
	// Default constructor
        Board( void );

	//Destructor
	~Board(void) {
	  for( char r='8' ; r>='1' ; r-- ) {
	     for( char c='A' ; c<='H' ; c++ ) {
	       if(_occ.find(std::make_pair(c,r)) != _occ.end()) {
		 if(_occ.at(std::make_pair(c,r)) != NULL) {
		   delete _occ.at(std::make_pair(c,r));
		 }
	       } 
	     }
	   }
	}
	

	// Returns a const pointer to the piece at a prescribed location if it exists, or a NULL pointer if there is nothing there.
	const Piece* operator() ( std::pair< char , char > position ) const;

	// Attempts to add a new piece with the specified designator, at the given location.
	// Returns false if:
	// -- the designator is invalid,
	// -- the specified location is not on the board, or
	// -- if the specified location is occupied
	bool add_piece( std::pair< char , char > position , char piece_designator );

	//Returns a reference to _occ
	std::map<std::pair<char,char>,Piece*>& get_board(); 
	    

	// Displays the board by printing it to stdout
	void display( void ) const;

	// Returns true if the board has the right number of kings on it
	bool has_valid_kings( void ) const;

	// Checks whether a designated straight line path on the board is clear (free of pieces)
	bool path_is_clear(std::pair<char,char> start, std::pair<char,char> end) const;

	//Attempts to remove existing piece from board
	bool remove_piece(std::pair< char , char > position);


 private:
	// The sparse map storing the pieces, keyed off locations
	std::map< std::pair< char , char > , Piece* > _occ;
};

// Write the board state to an output stream
std::ostream& operator << ( std::ostream& os , const Board& board );
#endif // BOARD_H
