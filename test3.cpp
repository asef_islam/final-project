#include "Board.h"
#include "CreatePiece.h"
#include <utility>
#include <cassert>
#include <iostream>
#include <string>

using std::string;
using std::make_pair;
using std::pair;
using std::cout;
using std::endl;

int main() {
  Board test_board;
  assert(test_board.add_piece(make_pair('F','2'),'q'));
  assert(test_board.add_piece(make_pair('C','3'),'Q'));
  assert(test_board.add_piece(make_pair('H','2'),'K'));
  assert(test_board.add_piece(make_pair('G','8'),'k'));
  assert(test_board.add_piece(make_pair('G','3'),'P'));
  assert(test_board.add_piece(make_pair('B','3'),'P'));
  assert(test_board.add_piece(make_pair('A','7'),'r'));
  assert(!test_board.add_piece(make_pair('Z','7'),'r'));
  assert(!test_board.add_piece(make_pair('A','9'),'r'));
  assert(!test_board.add_piece(make_pair('A','7'),'v'));
  assert(!test_board.add_piece(make_pair('A','0'),'p'));

  assert(test_board.has_valid_kings());

  Board test;
  assert(test.add_piece(make_pair('C','3'),'Q'));
  assert(test.add_piece(make_pair('H','2'),'K'));
  assert(!test.has_valid_kings());
  assert(!test_board.path_is_clear(make_pair('F','2'), make_pair('H','4')));
  assert(test_board.path_is_clear(make_pair('E','2'), make_pair('E','8')));
  assert(test_board.path_is_clear(make_pair('B','4'), make_pair('G','4')));
  assert(test_board.path_is_clear(make_pair('G','2'), make_pair('C','6')));
  assert(!test_board.path_is_clear(make_pair('C','1'), make_pair('C','6')));
  assert(!test_board.path_is_clear(make_pair('A','1'), make_pair('E','5')));
  assert(test_board.path_is_clear(make_pair('B','1'), make_pair('E','4')));
  cout << "all assertions passed!" << endl;
  test_board.display();
  cout << endl;
}
