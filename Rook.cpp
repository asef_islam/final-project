//Rook.cpp

#include "Rook.h"


//returns whether or not inputted move is valid for a rook
bool Rook::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //starting and ending locations must be different
  if(start == end) {
    return false;
  }
  //checks that end location is within the board boundaries
  if(end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  //conditions for a rook move to be valid
  //end location is directly vertical or horizontal to start by 1 or more spaces
  if(((start.first > end.first || start.first < end.first) &&  start.second == end.second) ||
     ((start.second > end.second || start.second < end.second) &&  start.first == end.first)) {
    return true;
  }
  else { //conditions not met, move is invalid
    return false;
  }
}
