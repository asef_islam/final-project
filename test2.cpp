#include "CreatePiece.h"
#include <utility>
#include <cassert>
#include <iostream>
#include <string>
#include "Chess.h"

using std::make_pair;
using std::pair;
using std::cout;
using std::endl;
using std::string;

int main() {
  Chess chess;
  chess.board().display();
  cout << endl;

  assert(!chess.in_check(false));

  cout << "moving white pawn in crazy move" << endl;
  cout << chess.board()(make_pair('A','2'))->legal_move_shape(make_pair('A','2'),make_pair('G','3')) << endl;
  assert(!chess.make_move(make_pair('A','2'), make_pair('G','3')));
  chess.board().display();
  cout << endl;
  
  cout << "moving white pawn one space forward" << endl;
  assert(chess.make_move(make_pair('A','2'), make_pair('A','3')));
  chess.board().display();
  cout << endl;

  cout << "moving black knight in valid move shape" << endl;
  assert(chess.make_move(make_pair('B','8'), make_pair('C','6')));
  chess.board().display();
  cout << endl;
  
  cout << "attempting to move white rook through white pawn" << endl;
  assert(!chess.make_move(make_pair('H','1'), make_pair('H','5')));
  chess.board().display();
  cout << endl;

  cout << "moving white pawn" << endl;
  assert(chess.make_move(make_pair('F','2'), make_pair('F','3')));
  chess.board().display();
  cout << endl;

  cout << "moving black pawn" << endl;
  assert(chess.make_move(make_pair('E','7'), make_pair('E','6')));
  chess.board().display();
  cout << endl;

  cout << "moving white pawn" << endl;
  assert(chess.make_move(make_pair('F','3'), make_pair('F','4')));
  chess.board().display();
  cout << endl;

  cout << "moving black bishop near king" << endl;
  assert(chess.make_move(make_pair('F','8'), make_pair('B','4')));
  chess.board().display();
  cout << endl;

  cout << "attempting to move white pawn in way that would result in check for white king" << endl;
  assert(!chess.make_move(make_pair('D','2'), make_pair('D','3')));
  chess.board().display();
  cout << endl;

  cout << "all assertions passed!" << endl;
}



