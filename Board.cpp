#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"
#include <string>
#include <iostream>
#include <cctype>

using std::string;
using std::pair;
using std::isupper;
using std::cout;
using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

const Piece* Board::operator()( std::pair< char , char > position ) const
{
  if(_occ.find(position) == _occ.end()) { //no piece there
    return NULL;
  }
  return _occ.at(position); 
}

bool Board::add_piece( std::pair< char , char > position , char piece_designator ) {
  string valid_pieces = "KkQqBbRrPpNnMm";
  if(valid_pieces.find(piece_designator) == string::npos) { //if piece design not among valid ones
     return false;
  }
  if(position.first < 'A' || position.first > 'H' || position.second < '1' || position.second > '8') { //position not on board 
     return false;
  }
  if(operator()(position) != NULL) { //piece already exists at that position
     return false;
  }
  _occ[ position ] = create_piece( piece_designator ); //piece is created at that location   
    return true; 
}


  
bool Board::remove_piece(std::pair< char , char > position) {
  //no piece exists at specified location
  if(operator()(position) == NULL) {
    return false;
  }
  _occ.erase(position);
  return true;
}


bool Board::has_valid_kings( void ) const{
  int wk = 0; //white king
  int bk = 0; //black king
  for(std::map<pair<char,char>, Piece*>::const_iterator it = _occ.begin(); it != _occ.end(); ++it) {
    if(it->second->to_ascii() == 'K') { //white king is found
      wk = 1;
    }
    else if(it->second->to_ascii() == 'k') { //black king is found
      bk = 1;
    }
  }
  if(wk+bk == 2) { //total 2 kings are on the board
    return true;
  }
  else {
    return false;
  } 
}


bool Board::path_is_clear(std::pair<char,char> start, std::pair<char,char> end) const {
  if (abs(start.first-end.first) <= 1 && abs(start.second-end.second) <= 1) { //only stepping one in either direction
    return true;
  }
  else if (start.first == end.first) { // staying in same column
    if (start.second < end.second) {
      for (int i = 1; i < end.second - start.second; i++) {
	if(operator()((std::pair<char,char>(start.first,start.second+i)))) {
	  return false;
	}
      }
    }
    else {
      for (int i = 1; i < start.second - end.second; i++) {
        if(operator()((std::pair<char,char>(start.first,end.second+i)))) {
          return false;	
	}
      }
    }
    return true;
  }
  else if (start.second == end.second) { // staying in same row          
    if (start.first < end.first) {
      for (int i = 1; i < end.first - start.first; i++) {
        if(operator()((std::pair<char,char>(start.first+i,start.second)))) {
          return false;	
	}
      }
    }
    else {
      for (int i = 1; i < start.first - end.first; i++) {
	if(operator()((std::pair<char,char>(end.first+i,end.second)))) {
          return false;
        }
      }
    }
    return true;
  }
  else if (abs(start.first-end.first) == abs(start.second-end.second)) { //diagonal
    if (end.first > start.first && end.second > start.second) { //moving up, right
      for (int i = 1; i < abs(start.first-end.first); i++) {
	if(operator()((std::pair<char,char>(start.first+i,start.second+i)))) {
	  return false;
	}
      }
    }
    else if (end.first > start.first && end.second < start.second) { //moving down, right
      for (int i = 1; i < abs(start.first-end.first); i++) {
	if(operator()((std::pair<char,char>(start.first+i,start.second-i)))) {
          return false;	
	}
      }
    }
    else if (end.first < start.first && end.second > start.second) { //moving up, left
      for (int i = 1; i < abs(start.first-end.first); i++) {
        if(operator()((std::pair<char,char>(start.first-i,start.second+i)))) {
          return false;
        }
      }
    }
    else if (end.first < start.first && end.second < start.second) { //moving down, left 
      for (int i = 1; i < abs(start.first-end.first); i++) {
        if(operator()((std::pair<char,char>(start.first-i,start.second-i)))) {
          return false;
        }
      }
    }
    return true;
  }
  else {
    return true;
  }
    
}
void Board::display( void ) const {
  cout << endl;
  int row_num = 2; //for alternating pattern
  for(char r = '8'; r >= '1'; r--) {
    cout << r; //each row number printed
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = operator()((std::pair<char,char>(c , r)));
      if((row_num % 2) == 0) { //every other row
	if((c % 2) == 0) {
	  Terminal::color_bg(Terminal::RED);
	}
	else {
	  Terminal::color_bg(Terminal::GREEN);
	}
      }
      else {
	if((c % 2) == 0) { //every other row
	  Terminal::color_bg(Terminal::GREEN);
	}
	else {
	  Terminal::color_bg(Terminal::RED);
	}
      }
      if(piece) { //piece is at location
	if(isupper(piece->to_ascii())) { //white piece
	  Terminal::color_fg(true, Terminal::WHITE);
	  cout << " " << piece->to_ascii() << " ";
	  Terminal::set_default();
	}
	else { //black piece
	  Terminal::color_fg(false, Terminal::BLACK);
	  cout << " " << piece->to_ascii() << " ";
	  Terminal::set_default();
	}
      }
      else { //no piece, empty space
	Terminal::color_fg(true, Terminal::WHITE);
	cout << " - ";
	Terminal::set_default();
      }
    }
    cout << endl;
    row_num++;
  }
  //column letter printed out
  cout << ' ' << " A " << " B " << " C " << " D " << " E " << " F " << " G " << " H " << endl;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}
